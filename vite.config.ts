import path from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { VitePWA } from 'vite-plugin-pwa';
import proxy from './proxy';

// https://vitejs.dev/config/
export default defineConfig({
    base: process.env.PUBLIC_PATH || '/',
    define: {
        'process.env': {},
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
    },
    plugins: [
        vue(),
        VitePWA({
            filename: '',
            manifest: {
                name: 'manifest.json',
            },
            workbox: {
                globIgnores: ['**/api.json'],
                runtimeCaching: [
                    {
                        urlPattern: /fonts/i,
                        handler: 'CacheFirst',
                        options: {
                            cacheName: 'fonts-cache',
                            expiration: {
                                maxEntries: 10,
                                maxAgeSeconds: 60 * 60 * 24 * 365, // <== 365 days
                            },
                            cacheableResponse: {
                                statuses: [0, 200],
                            },
                        },
                    },
                ],
            },
        }),
    ],
    server: {
        port: 8084,
        proxy,
    },
});
