/* eslint-disable global-require */
/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
const proxy = require('./proxy');
const packages = require('./package.json');

/**
 * https://cli.vuejs.org/zh/config/
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    publicPath: process.env.PUBLIC_PATH || '/',
    outputDir: 'dist',
    assetsDir: 'static',
    runtimeCompiler: false,
    transpileDependencies: [],
    productionSourceMap: false,
    lintOnSave: true,
    css: {
        extract: true,
        sourceMap: true,
        // modules: true,
        requireModuleExtension: true,
        loaderOptions: {
            css: {
                modules: {
                    localIdentName: '[local]_[hash:base64:8]', // 设定 CSS Modules 命名规则
                },
                localsConvention: 'camelCaseOnly',
            },
            less: {
                modules: {
                    localIdentName: '[local]_[hash:base64:8]', // 设定 CSS Modules 命名规则
                },
                localsConvention: 'camelCaseOnly',
            },
        },
    },
    configureWebpack: {
        resolve: {
            extensions: ['.ts', '.js', '.vue', '.json'],
            // 配置别名
            alias: {
                '@': path.join(__dirname, 'src'),
            },
        },
        plugins: [
            new SWPrecacheWebpackPlugin({
                cacheId: packages.name,
                filename: 'service-worker.js',
                maximumFileSizeToCacheInBytes: 4194304,
                minify: true,
                runtimeCaching: [{
                    handler: 'cacheFirst',
                    urlPattern: /[.]json$/,
                }],
            }),
        ],
    },

    chainWebpack: (config) => {
        config.externals = {
            vue: 'Vue',
            axios: 'axios',
            iview: 'iview',
        };
        config.optimization.minimizer('terser').tap((args) => {
            args[0].terserOptions.compress.drop_console = true;
            return args;
        });
        // 移除 prefetch 插件
        config.plugins.delete('manifest');
        config.plugins.delete('preload');
        config.plugins.delete('prefetch');
        config.plugins.delete('workbox');
    },

    // 是否为 Babel 或 TypeScript 使用 thread-loader。该选项在系统的 CPU 有多于一个内核时自动启用，仅作用于生产构建。
    parallel: require('os').cpus().length > 1,

    // 向 PWA 插件传递选项，会引入 <link rel="icon" type="image/png" sizes="32x32" href="/img/icons/favicon-32x32.png">
    // https://github.com/vuejs/vue-cli/tree/dev/packages/@vue/cli-plugin-pwa
    // https://blog.csdn.net/tangkthh/article/details/109233438
    pwa: {
        iconPaths: {
            favicon32: './favicon.ico',
            favicon16: './favicon.ico',
            appleTouchIcon: './favicon.png',
            maskIcon: './favicon.png',
            msTileImage: './favicon.png',
        },
    },

    devServer: {
        host: '127.0.0.1',
        disableHostCheck: true,
        port: 8083,
        https: false,
        open: false, // 配置自动启动浏览器  open: 'Google Chrome'-默认启动谷歌

        // 配置多个代理
        proxy,
    },
};
