module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/vue3-essential',
        '@vue/airbnb',
        '@vue/typescript/recommended',
    ],
    parserOptions: {
        ecmaVersion: 2020,
    },
    rules: {
        semi: ['error', 'always'],
        indent: ['error', 4],
        camelcase: 0,
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-var': ['error'],
        'quote-props': ['error', 'as-needed'],
        'space-before-function-paren': ['error', 'never'],
        'comma-dangle': ['error', 'always-multiline'],
        // 'import/no-cycle': 0,
        // 'import/no-extraneous-dependencies': 0,
        'no-param-reassign': 0,
        'no-plusplus': 0,
        'no-restricted-syntax': 0,
        'import/prefer-default-export': 0,
        'guard-for-in': 0,
        'default-case': 0,
        'func-names': 0,
        'consistent-return': 0,
        'prefer-regex-literals': 0,
        'max-len': ['error', 180, 2],
        'vuejs-accessibility/click-events-have-key-events': 0,
        'import/no-extraneous-dependencies': [2, { devDependencies: true }],
        'import/no-unresolved': [2, { ignore: ['antd-mobile', '@', './'] }],
        'import/extensions': ['error', {
            ignore: ['^@/'], js: 'never', less: 'always', ts: 'never', vue: 'always', json: 'always',
        }],
        'no-nested-ternary': 0,
        'no-underscore-dangle': 0,
        'vue/no-parsing-error': [2, { 'x-invalid-end-tag': false }],
        // 不用直接声明返回类型，函数返回值自己推断
        '@typescript-eslint/explicit-module-boundary-types': 0,
    },
    overrides: [
        {
            files: [
                '**/__tests__/*.{j,t}s?(x)',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
            ],
            env: {
                mocha: true,
            },
        },
    ],
};
