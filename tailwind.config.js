/**
 * https://www.tailwindcss.cn/docs/configuration
 */
module.exports = {
    purge: [
        './*.html',
        './public/*.html',
        './src/**/*.html',
        './src/**/*.vue',
        './src/**/*.js',
        './src/**/*.ts',
    ],
    darkMode: false, // or 'media' or 'class'
    important: true,
    theme: {
        extend: {},
    },
    variants: {},
    plugins: [],
};
