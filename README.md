# `Vue3` Scaffolding: including both `@vue/cli` and `Vite`

## Development

```bash
## Project setup
yarn install
### Compiles and hot-reloads for development
yarn serve
### Compiles and minifies for production
yarn build
### Run your unit tests
yarn test:unit
### Run your end-to-end tests
yarn test:e2e
### Lints and fixes files
yarn lint
```

## Configuration

See [@vue/cli configuration](https://cli.vuejs.org/config/)
See [vitejs configuration](https://cn.vitejs.dev/config/)
