/**
 * https://cli.vuejs.org/zh/config/
 * @type {import('@types/webpack-dev-server').ProxyConfigMap}
 */
module.exports = {
    '/api/': {
        target: process.env.API_HOST || 'http://localhost:16003',
        pathRewrite: { '/api': '' }, // used by webpack-dev-server https://webpack.js.org/configuration/dev-server/#devserverproxy
        rewrite: (path) => path.replace(/^\/api/, ''), // used by vite https://vitejs.dev/config/#server-proxy
        changeOrigin: true, // 允许websockets跨域
    },
};
